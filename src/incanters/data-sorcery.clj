;;;; data sorcery site and presentation ;;;;
;; http://incanter.org/docs/data-sorcery-light-new.pdf


;;; Creating datasets
(def ds1 (dataset ["x1" "x2" "x3"]
                  [[1 2 3]
                   [4 5 6]
                   [7 8 9]]))
;; Deprecated. Please use clojure.core.matrix.dataset/dataset instead.

(def ds2 (to-dataset [{"x1" 1, "x2" 2, "x3" 3}
                      {"x1" 4, "x2" 5, "x3" 6}
                      {"x1" 7, "x2" 8, "x3" 9}]))

(conj-cols [1 4 7] [2 5 8] [3 6 9])
;; => 
| 0 | 1 | 2 |
|---+---+---|
| 1 | 2 | 3 |
| 4 | 5 | 6 |
| 7 | 8 | 9 |

(def cars (get-dataset :cars))
(def x (sel cars :cols 0))
(view (conj-cols cars cars))
(view (conj-cols cars x))
(view (conj-cols (range (nrow cars)) cars))
(view (conj-cols (range 10) (range 10)))


(conj-rows [[1 2] [3 4]]
           [[5 6] [7 8]]
           [[5 6] [7 8]])
(conj-rows [[1 2 3 4] [4 5 6 7]]
           [[1 2 3 4] [4 5 6 7]])
(conj-rows [{:a 1 :b 2} {:a 3 :b 4}]
           [{:a 5 :b 6} {:a 7 :b 8}])


;; Reading data
(def car-dataset (read-dataset "resources/cars.csv" :header true))

;; Column selection
($ :speed (get-dataset :cars))

(with-data (get-dataset :cars)
  ["mean = "(mean ($ :speed))
   "sd = "(sd ($ :speed))])

(with-data (get-dataset :iris)
  (view $data)
  (view ($ [:Sepal.Length :Sepal.Width :Species]))
  (view ($ [:not :Petal.Width :Petal.Length]))
  (view ($ 2 [:not :Petal.Width :Petal.Length])))


;; Row selection
(def iris (get-dataset :iris))
($where {:Species "setosa"}
        iris)
($where {:Petal.Length {:lt 2}}
        iris)
($where {:Petal.Width {:gt 1.0 :lt 1.5}}
        iris)
($where {:Petal.Width {:gt 0.3 :lt 1.5}
         :Species {:in #{"setosa" "virginica"}}}
        iris)
($where #(or (< (% :Petal.Width) 1.0)
             (> (% :Petal.Length) 5.0))
        iris)
($where (fn [row]
          (or (< (row :Petal.Width) 1.0)
              (> (row :Petal.Length) 5.0)))
        iris)


;; Sorting data
(def hair-eye-color (get-dataset :hair-eye-color))
(view hair-eye-color)
(view ($order :count :desc hair-eye-color))
(view ($order [:hair :eye] :asc hair-eye-color))


;; Rolling up data
(view ($rollup mean :Petal.Length :Species iris))
(->> iris
     ($rollup #(/ (sd %) (count %))
              :Petal.Length :Species)
     view)
(->> hair-eye-color
     ($rollup sum :count [:hair :eye])
     ($order :count :desc)
     view)


;; Charts
(view (scatter-plot :Sepal.Length :Sepal.Width
                    :data iris))
(view (scatter-plot :Sepal.Length :Sepal.Width
                    :group-by :Species
                    :title "Fisher Iris Data"
                    :x-label "Sepal Length (cm)"
                    :y-label "Sepal Width (cm)"
                    :data iris))
(with-data iris
  (doto
      (scatter-plot :Petal.Width :Petal.Length
                    :data ($where {:Petal.Length {:lte 2.0}
                                   :Petal.Width {:lt 0.75}}))
    (add-points :Petal.Width :Petal.Length
                :data ($where {:Petal.Length {:gt 2.0}
                               :Petal.Width {:gte 0.75}}))
    view))
(with-data iris
  (let [lm (linear-model ($ :Petal.Length) ($ :Petal.Width))]
    (doto
             (scatter-plot :Petal.Width :Petal.Length
                           :data ($where {:Petal.Length {:lte 2.0}
                                          :Petal.Width {:lt 0.75}}))
             (add-points :Petal.Width :Petal.Length
                         :data ($where {:Petal.Length {:gt 2.0}
                                        :Petal.Width {:gte 0.75}}))
             (add-lines :Petal.Width (:fitted lm))
             view)))

(with-data ($rollup mean :Sepal.Length :Species
                     iris)
  (view (bar-chart :Species :Sepal.Length)))

(with-data ($rollup mean :Sepal.Length :Species
                    iris)
  (view (line-chart :Species :Sepal.Length)))

(with-data ($rollup :mean :count [:hair :eye]
                    hair-eye-color)
  (view $data)
  (view (bar-chart :hair :count
                   :group-by :eye
                   :legend true
                   :theme :dark)))

(with-data ($rollup :mean :count [:hair :eye]
                    hair-eye-color)
  (view $data)
  (view (bar-chart :hair :count
                   :group-by :eye
                   :legend true)))

(with-data ($rollup :mean :count [:hair :eye]
                    hair-eye-color)
  (view $data)
  (view (line-chart :hair :count
                    :group-by :eye
                    :legend true)))

(with-data (->>  hair-eye-color
                 ($where {:hair {:in #{"brown" "blond"}}})
                 ($rollup :sum :count [:hair :eye])
                 ($order :count :desc))
  (view $data)
  (view (bar-chart :hair :count
                   :group-by :eye
                   :legend true)))
(with-data (->>  hair-eye-color
                 ($where {:hair {:in #{"brown" "blond"}}})
                 ($rollup :sum :count [:hair :eye])
                 ($order :count :desc))
  (view $data)
  (view (line-chart :hair :count
                   :group-by :eye
                   :legend true)))

(with-data (get-dataset :cars)
  (view (xy-plot :speed :dist)))

(defn cubic [x] (+ (* x x x) (* 2 x x) (* 2 x) 3))
(doto (function-plot cubic -10 +10)
  view)
(doto (function-plot cubic -10 +10)
  (add-function (derivative cubic) -5 +5)
  view)

(doto (histogram (sample-gamma 1000)
                 :density true
                 :nbins 30)
  (add-function pdf-gamma 0 8)
  view)

(with-data iris
  (view (box-plot :Petal.Width
                  :group-by :Species)))

;; Annotating charts
(doto (function-plot sin -10 +10)
  (add-text 0 0 "text at 0,0")
  (add-pointer (- Math/PI) (sin (- Math/PI))
               :text "pointer at sin -pi")
  (add-pointer Math/PI (sin Math/PI)
               :text "pointer at(sin pi)"
               :angle :north)
  view)

