(ns incanter.core
  (:require [incanter [core :refer :all]
             [stats :refer :all]
             [charts :refer :all]
             [datasets :refer :all]
             [io :refer :all]
             [pdf :refer :all]]
            [clojure.data.json :as json]))

;; sample 1,000 values from a standard-normal distribution and view a histogram
(view (histogram (sample-normal 1000)))

;; a plot of the sine function over the range -10 to 10
(def sin-plot (function-plot sin -10 10))
(view sin-plot)
(save-pdf sin-plot "src/incanter/sin.pdf")


;;;; ch6 incanter datasets from clojure data analysis cookbook by packt ;;;;

(def iris-hist
  (histogram :Sepal.Length
             :data iris))
(view iris-hist)

(with-data (get-dataset :iris)
  (view (histogram :Petal.Length)))


;; convert data into dataset
(def matrix-set (to-dataset [[1 2 3] [4 5 6]]))
(nrow matrix-set)
;; => 2
(col-names matrix-set)
;; => [0 1 2]

(def maps-set (to-dataset [{:a 1, :b 2, :c 3}
                           {:a 4, :b 5, :c 6}]))
(nrow maps-set) 
;; => 2
(col-names maps-set)
;; => [:c :b :a]

(def matrix-set-2
  (dataset [:a :b :c] ;; can't be to-dataset which uses dataset when more control is required
           [[1 2 3]
            [4 5 6]
            [7 8 9]]))
matrix-set-2
;; => | :a | :b | :c |
;; => |----+----+----|
;; => |  1 |  2 |  3 |
;; => |  4 |  5 |  6 |
;; => |  7 |  8 |  9 |

(nrow matrix-set-2)
;; => 3
(ncol matrix-set-2)
;; => 3
(col-names matrix-set-2)
;; => [:a :b :c]


;; Viewing datasets interactively with view
(def iris (get-dataset :iris))
(view iris)
(col-names iris)
(:shape iris)
(nrow iris)
(set ($ :Species iris))


;; Converting datasets to matrices
(def data-file "http://www.ericrochester.com/clj-data-analysis/data/all_160_in_51.P35.csv")
(def va-data (read-dataset data-file :header true))
(view va-data)
(col-names va-data)

(def va-matrix
  (to-matrix ($ [:POP100 :HU100 :P035001] va-data)))
(view va-matrix)

(first va-matrix)
;; => #vectorz/array [8191.0,4271.0,2056.0]
(take 5 va-matrix)
;; => (#vectorz/array [8191.0,4271.0,2056.0]
;;     #vectorz/array [519.0,229.0,117.0]
;;     #vectorz/array [298.0,163.0,77.0]
;;     #vectorz/array [139966.0,72376.0,30978.0]
;;     #vectorz/array [117.0,107.0,32.0])
(nrow va-matrix);; => 591
(reduce plus va-matrix)
;; => #vectorz/vector [5433225.0,2262695.0,1332421.0]
(mapv mean (clojure.core.matrix/transpose va-matrix))
;; => [9193.274111675128 3828.587140439932 2254.5194585448394]
(mapv mean (trans va-matrix))
;; => [9193.274111675128 3828.587140439932 2254.5194585448394]
($= (reduce plus va-matrix) / (nrow va-matrix))
;; => #vectorz/vector [9193.274111675126,3828.587140439932,2254.5194585448394]
(covariance (matrix va-matrix))
;; => #vectorz/matrix [[8.650826158128713E8,3.6407022927437824E8,2.0879877071838593E8],
;; =>[3.6407022927437824E8,1.553507003546817E8,8.729635653686081E7],
;; =>[2.0879877071838593E8,8.729635653686082E7,5.112514534156987E7]]
;; not sure what this is

(mult 4 va-matrix)
($= va-matrix * 4)

;; Using infix formulas in Incanter
($= 7 * 4)
(macroexpand-1 '($= 7 * 4));; => (incanter.core/mult 7 4)
($= 7 * 4 + 3)
(macroexpand-1 '($= 7 * 4 + 3));; => (incanter.core/plus (incanter.core/mult 7 4) 3)

;; Selecting columns with $
(def race-data (read-dataset
                "http://www.ericrochester.com/clj-data-analysis/data/all_160.P3.csv"
                :header true))
(col-names race-data)
($ :POP100 race-data)
($ [:POP100] race-data)
($ [:STATE :POP100 :POP100.2000] race-data)

(def col-info (read-dataset
               "https://raw.github.com/ireapps/census/master/tools/metadata/sf1_labels.csv"))

;; Selecting rows with $
($ 0 :all race-data)
;; => [100100 160 1 nil nil nil nil nil "Abanda CDP" 192 79 nil nil 192 nil 129 nil 58 nil 0 nil 0 nil 0 nil 2 nil 3 nil]
($ 0
   [:STATE :POP100 :P003002 :P003003 :P003004 :P003005 :P003006 :P003007 :P003008]
   race-data)
;; => [1 192 129 58 0 0 0 2 3]

($ [0 1] :all race-data)
($ [0 1 2 3 4]
   [:STATE :POP100 :P003002 :P003003 :P003004 :P003005 :P003006 :P003007 :P003008]
   race-data)

;; Filtering datasets with $where
(def richmond ($where {:NAME "Richmond city"} va-data))
(def small ($where {:POP100 {:lte 1000}} va-data))
($ [0 1 2 3] :all small)
(def medium ($where {:POP100 {:gt 1000 :lt 4000}} va-data))
($ [0 1 2 3] :all medium)
(def random-half
  ($where {:GEOID {:fn (fn [_] (< (rand) 0.5))}}
          va-data))

;; Grouping data with $group-by
(def by-state ($group-by :STATE race-data))
(take 5 by-state)
($ [0 1 2 3] :all (by-state {:STATE 51}))


;; Saving datasets to CSV and JSON
(def census2010 ($ [:STATE :NAME :POP100 :P003002 :P003003 :P003004 :P003005 :P003006 :P003007 :P003008]
                   race-data))
(with-open [f-out (clojure.java.io/writer "src/cookbook/census-2010.csv")]
  (clojure.data.csv/write-csv f-out [(map name (col-names census2010))])
  (clojure.data.csv/write-csv f-out (to-list census2010)))
(with-open [f-out (clojure.java.io/writer "src/cookbook/census-2010.json")]
  (json/write (:rows census2010) f-out)) ; needed clojure.data.json entry in ns for some reason

;; Projecting from multiple datasets with $join
(def family-data
  (read-dataset "http://www.ericrochester.com/clj-data-analysis/data/all_160_in_51.P35.csv"
                :header true))
(def racial-data
  (read-dataset "http://www.ericrochester.com/clj-data-analysis/data/all_160_in_51.P3.csv"
                :header true))
(clojure.set/intersection (set (col-names family-data))
                          (set (col-names racial-data)))

(defn dedup-second
  [a b id-col]
  (let [a-cols (set [col-names a])]
    (conj (filter #(not (contains? a-cols %))
                  (col-names b))
          id-col)))

(def racial-short ($ (vec (dedup-second family-data
                                        racial-data
                                        :GEOID))
                     racial-data))

(def all-data
  ($join [:GEOID :GEOID] family-data racial-short))
