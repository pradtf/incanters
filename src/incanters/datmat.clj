;;;; dataset and matrix xpt ;;;;
;;; https://github.com/incanter/incanter/wiki
;;; see updates to incanter 2.0 https://github.com/incanter/incanter/wiki/Incanter-2.0-change-log

(ns datmat
  (:require [clojure.core.matrix.dataset :as ds]           
            [clojure.core.matrix :as mt]
            (incanter [core :refer :all]
                      [stats :refer :all]
                      [charts :refer :all]
                      [datasets :refer :all]
                      [io :refer :all])))

(def iris (get-dataset :iris))
(head 2 iris)
;; => 
| :Sepal.Length | :Sepal.Width | :Petal.Length | :Petal.Width | :Species |
|---------------+--------------+---------------+--------------+----------|
|           5.1 |          3.5 |           1.4 |          0.2 |   setosa |
|           4.9 |          3.0 |           1.4 |          0.2 |   setosa |

(def cars (get-dataset :cars))
(head 2 cars)
;; => 
| :speed | :dist |
|--------+-------|
|    4.0 |   2.0 |
|    4.0 |  10.0 |

(def ticsORG (read-dataset "/zata/truefx/USDCAD/USDCAD-2018-12.csv"))
(head 2 ticsORG)
;; => 
|   :col0 |                 :col1 |   :col2 |   :col3 |
|---------+-----------------------+---------+---------|
| USD/CAD | 20181202 22:04:20.855 | 1.32333 |   1.325 |
| USD/CAD | 20181202 22:04:39.712 | 1.32333 | 1.32438 |
(def tics (rename-cols {:col1 :datetime
                        :col2 :bid
                        :col3 :ask}
                       (ds/remove-columns ticsORG [:col0])))
(head 2 tics)
;; => 
|             :datetime |    :bid |    :ask |
|-----------------------+---------+---------|
| 20181202 22:04:20.855 | 1.32333 |   1.325 |
| 20181202 22:04:39.712 | 1.32333 | 1.32438 |



(def ohlcORG (read-dataset "/zata/truefx/USDCAD/usdcad4H.csv"))
(head 2 ohlcORG)
;; => 
|               :col0 |   :col1 |   :col2 |   :col3 |   :col4 |
|---------------------+---------+---------+---------+---------|
| 2011-01-02 20:00:00 | 0.99485 | 0.99492 | 0.99292 | 0.99298 |
| 2011-01-03 00:00:00 | 0.99302 | 0.99494 | 0.99264 | 0.99271 |
(def ohlc (rename-cols {:col0 :date-time
                        :col1 :open
                        :col2 :high
                        :col3 :low
                        :col4 :close}
                       ohlcORG))
(head 2 ohlc)
;; => 
|          :date-time |   :open |   :high |    :low |  :close |
|---------------------+---------+---------+---------+---------|
| 2011-01-02 20:00:00 | 0.99485 | 0.99492 | 0.99292 | 0.99298 |
| 2011-01-03 00:00:00 | 0.99302 | 0.99494 | 0.99264 | 0.99271 |
(def loch (reorder-columns ohlc [:date-time :low :open :close :high]))
(head 2 loch)
;; => 
|          :date-time |    :low |   :open |  :close |   :high |
|---------------------+---------+---------+---------+---------|
| 2011-01-02 20:00:00 | 0.99292 | 0.99485 | 0.99298 | 0.99492 |
| 2011-01-03 00:00:00 | 0.99264 | 0.99302 | 0.99271 | 0.99494 |



(def iris-mat (to-matrix iris))
(head 2 iris-mat)
;; => #vectorz/matrix
[[5.1,3.5,1.4,0.2,0.0],
 [4.9,3.0,1.4,0.2,0.0]]

;;not sure why this should be done??
(def iris-dummy (to-matrix iris :dummies true))
(head 2 iris-dummy)
;; => #vectorz/matrix
[[5.1,3.5,1.4,0.2,0.0,0.0],
 [4.9,3.0,1.4,0.2,0.0,0.0]]

(def A (matrix [[1 2 3] [4 5 6] [7 8 9]])) ; produces a 3x3 matrix
(def A2 (matrix [1 2 3 4 5 6 7 8 9] 3)) ; produces the same 3x3 matrix
(def B (matrix [1 2 3 4 5 6 7 8 9])) ; produces a 9x1 column vector

(matrix 0 3 4)
;; => #vectorz/matrix
[[0.0,0.0,0.0,0.0],
 [0.0,0.0,0.0,0.0],
 [0.0,0.0,0.0,0.0]]

(mt/identity-matrix 4) ;incanter's identity matrix deprecated
;; => #vectorz/matrix
[[1.0,0.0,0.0,0.0],
 [0.0,1.0,0.0,0.0],
 [0.0,0.0,1.0,0.0],
 [0.0,0.0,0.0,1.0]]

(diag [1 2 3 4]) ;incanter's diag deprecated
;; => #vectorz/matrix
[[1.0,0.0,0.0,0.0],
 [0.0,2.0,0.0,0.0],
 [0.0,0.0,3.0,0.0],
 [0.0,0.0,0.0,4.0]]
(mt/diagonal-matrix [1 2 3 4])
;; => #vectorz/matrix
[[1.0,0.0,0.0,0.0],
 [0.0,2.0,0.0,0.0],
 [0.0,0.0,3.0,0.0],
 [0.0,0.0,0.0,4.0]]
(mt/main-diagonal [1 2 3 4]) 
;; => [1 2 3 4]

(mt/main-diagonal A)
;; => #vectorz/vector [1.0,5.0,9.0]

;; plus minus mult div abs exp sqrt pow cos acos sin asin tan atan sum prod
(mt/add A A2)
;; => #vectorz/matrix
[[2.0,4.0,6.0],
 [8.0,10.0,12.0],
 [14.0,16.0,18.0]]
(mt/sub A A2)
;; => #vectorz/matrix
[[0.0,0.0,0.0],
 [0.0,0.0,0.0],
 [0.0,0.0,0.0]]
(mt/add 11 A)
;; => #vectorz/matrix
[[12.0,13.0,14.0],
 [15.0,16.0,17.0],
 [18.0,19.0,20.0]]
(mt/mul A A2)
;; => #vectorz/matrix
[[1.0,4.0,9.0],
 [16.0,25.0,36.0],
 [49.0,64.0,81.0]]
(mt/mmul A A2)
;; => #vectorz/matrix
[[30.0,36.0,42.0],
 [66.0,81.0,96.0],
 [102.0,126.0,150.0]]
(mt/det A)
;; => 0.0

(first A)
;; => #vectorz/vector [1.0,2.0,3.0]
(rest A)
;; => (#vectorz/vector [4.0,5.0,6.0] #vectorz/vector [7.0,8.0,9.0])

(reduce mt/add A)
;; => #vectorz/vector [12.0,15.0,18.0]
(reduce mt/mul A)
;; => #vectorz/vector [28.0,80.0,162.0]
(map mt/esum A)
;; => (6.0 15.0 24.0)
(filter #(> (nth (seq %) 1) 4) A)
;; => (#vectorz/vector [4.0,5.0,6.0] #vectorz/vector [7.0,8.0,9.0])


;; subsets of matrices
(sel A 0 0)
;; => 1.0
(sel A :rows 0 :cols 0)
;; => 1.0

(sel A :cols 2)
;; => #vectorz/vector [3.0,6.0,9.0]
(sel A :rows 1)
;; => #vectorz/vector [4.0,5.0,6.0]


;; add a calculated column
(head (ds/add-column cars "sums" (mt/add (sel cars :cols 0) (sel cars :cols 1))))
;; => 
| :speed | :dist | sums |
|--------+-------+------|
|    4.0 |   2.0 |  6.0 |
|    4.0 |  10.0 | 14.0 |
|    7.0 |   4.0 | 11.0 |
|    7.0 |  22.0 | 29.0 |
|    8.0 |  16.0 | 24.0 |
|    9.0 |  10.0 | 19.0 |
|   10.0 |  18.0 | 28.0 |
|   10.0 |  26.0 | 36.0 |
|   10.0 |  34.0 | 44.0 |
|   11.0 |  17.0 | 28.0 |

(head (ds/add-column cars :sums (mt/add (sel cars :cols 0) (sel cars :cols 1))))
;; => 
| :speed | :dist | :sums |
|--------+-------+-------|
|    4.0 |   2.0 |   6.0 |
|    4.0 |  10.0 |  14.0 |
|    7.0 |   4.0 |  11.0 |
|    7.0 |  22.0 |  29.0 |
|    8.0 |  16.0 |  24.0 |
|    9.0 |  10.0 |  19.0 |
|   10.0 |  18.0 |  28.0 |
|   10.0 |  26.0 |  36.0 |
|   10.0 |  34.0 |  44.0 |
|   11.0 |  17.0 |  28.0 |


(defn sel-col
  "selects indicated columns from dataset or all columns if none provided"
  ([ds]
   (sel ds :cols (range (ncol ds))))
  ([ds & col-keys]
   (sel ds :cols col-keys)))

;; add calculated column and read it in view by making it matrix within dataset
(view (ds/add-column cars :tmp (to-matrix (mt/sub (sel-col cars :speed)
                                                  (sel-col cars :dist)))))

; xpt with headtics
(def headtics (head tics))
(def tics1 (ds/add-column headtics :spread
                                (to-matrix (mt/round
                                            (mt/mul 10000
                                                    (mt/sub (sel-col headtics :ask)
                                                            (sel-col headtics :bid)))))))
(def tics2 ((fn [x] ;not much of an improvement over the first
              (ds/add-column x :spread
                             (mt/round
                              (mt/mul 10000
                                      (mt/sub (sel-col x :ask)
                                              (sel-col x :bid))))))
            headtics))
(view tics1)


;; efficient ways of manipulating the dataset

;; get the spread added to tics
(def tics-spread
  (lazy-seq (ds/add-column tics :spread
                  (to-matrix (mt/round
                              (mt/mul 10000
                                      (mt/sub (sel-col tics :ask)
                                              (sel-col tics :bid))))))))

(view (take 10 tics-spread)) ;too too slow

;; changing to matrix seems to speed things up immensely
(def ticsm (to-matrix tics))
(def spread-col (mt/round (mt/mul 10000
                                  (mt/sub (sel ticsm :cols 2)
                                          (sel ticsm :cols 1)))))

;; this mechanism doing calculating in matrix and then joining to dataset is much faster!
(def tics-with-spread
  (let [ticsm (to-matrix tics)
        spread (mt/round
                (mt/mul 10000
                        (mt/sub (sel ticsm :cols 2)
                                (sel ticsm :cols 1))))]
    (ds/add-column tics :spread spread)))
(head tics-with-spread)

;; the slowdown seems to be in actually sel the column while it is a dataset
;; (time (def asks (sel tics :cols :ask))) went on for several minutes and we cut it off
;; however, (time (def asks (sel (to-matrix tics) :cols 2))) was done in just over 8s!
;; while the entire addition above required about 9.4s!




(view (ds/add-column headtics :spread (range 10)))
(view ((fn [x]
         (ds/add-column x :spread (range 10)))
       headtics))



(view (ds/add-column headtics :spread ))

(view (head tics-spread))

(sel-col iris)
(sel-col iris :Species)
(sel-col iris :Species :Sepal.Length :Petal.Length)





;; dataset
(to-dataset [[1 2 3] [4 5 6]])
;; => 
| 0 | 1 | 2 |
|---+---+---|
| 1 | 2 | 3 |
| 4 | 5 | 6 |

;; => 
| 0 | 1 | 2 |
|---+---+---|
| 1 | 2 | 3 |
| 4 | 5 | 6 |

;; => 
| 0 | 1 | 2 |
|---+---+---|
| 1 | 2 | 3 |
| 4 | 5 | 6 |

(matrix? iris)
;; => true

(mult (to-dataset [[1 2 3] [4 5 6]]) 3)
;; => 
|  0 |  1 |  2 |
|----+----+----|
|  3 |  6 |  9 |
| 12 | 15 | 18 |

(mult (to-dataset [[1 2 3] [4 5 6]]) [9 9 9])
;; => 
|  0 |  1 |  2 |
|----+----+----|
|  9 | 18 | 27 |
| 36 | 45 | 54 |

(mult (head 2 cars) 10)
;; => 
| :speed | :dist |
|--------+-------|
|   40.0 |  20.0 |
|   40.0 | 100.0 |

(ds/select-columns (head 2 iris) [:Species :Petal.Length])
;; => 
| :Species | :Petal.Length |
|----------+---------------|
|   setosa |           1.4 |
|   setosa |           1.4 |


;; save or append datmat to csv
(save iris "/tmp/iris.csv")
(save iris-mat "/tmp/iris-mat.csv"
      :header ["Sepal.Length" "Sepal.Width" "Petal.Length" "Petal.Width" "Species"])


;; many incanter functions have been replaced with equivalents from
;; clojure.core.matrix and clojure.core.matrix.dataset




;; incanter dataset
(def ticks-dataset (read-dataset csv-file))

(def ds999 (sel ticks-dataset :rows (range nrows)))

(with-data ds999
  (doto
      (scatter-plot (range nrows) ($ :col3))
    view))

(mean ($ :col2 ds999))
;; => 1.3250638838838842
(sd ($ :col2 ds999))
;; => 6.044039533359644E-4
(with-data ds999
  ["mean = " (mean ($ :col2 ds999))
   "stdv = " (sd ($ :col2 ds999))])
;; => ["mean = " 1.3250638838838842 "stdv = " 6.044039533359644E-4]

(head ticks-dataset)
;; => 
|   :col0 |                 :col1 |   :col2 |   :col3 |
|---------+-----------------------+---------+---------|
| USD/CAD | 20181202 22:04:20.855 | 1.32333 |   1.325 |
| USD/CAD | 20181202 22:04:39.712 | 1.32333 | 1.32438 |
| USD/CAD | 20181202 22:04:39.768 | 1.32338 | 1.32438 |
| USD/CAD | 20181202 22:04:48.737 | 1.32338 | 1.32503 |
| USD/CAD | 20181202 22:04:48.849 | 1.32327 | 1.32503 |
| USD/CAD | 20181202 22:04:51.633 | 1.32333 |   1.325 |
| USD/CAD | 20181202 22:05:01.765 | 1.32335 | 1.32485 |
| USD/CAD | 20181202 22:05:02.883 | 1.32334 | 1.32492 |
| USD/CAD | 20181202 22:05:03.884 |  1.3233 | 1.32492 |
| USD/CAD | 20181202 22:05:04.454 | 1.32338 | 1.32492 |

($ :col3 (head ticks-dataset))
;; => (1.325 1.32438 1.32438 1.32503 1.32503 1.325 1.32485 1.32492 1.32492 1.32492)

(def m999 (to-matrix ds999))
(clojure.core.matrix/esum ds999 )
(ds/column-names ds999)





;; diversion with ticks and read-csv
(def ticks-csv (clojure.data.csv/read-csv
                (slurp csv-file))) ;need to slurp file to read-csv

(def csv999 (sel ticks-csv :rows (range nrows))) ;just a collection of strings - no columns even
(def ds-from-csv999 (to-dataset ds999)) ;change it to dataset and it is same as ds999

