(ns incanters.core
  (:use (incanter core io stats charts optimize datasets)))

(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World!"))
