;;;; Working with data sets in Clojure with Incanter and MongoDB ;;;;
;;; Working with data sets in Clojure with Incanter and MongoDB

(def data (get-dataset :cars))
(dim data)
(col-names data)
(view data)

(with-data data
  (def lm (linear-model ($ :dist) ($ :speed)))
  (doto (scatter-plot ($ :speed) ($ :dist))
    (add-lines ($ :speed) (:fitted lm))
    view))

(with-data data
  (view (conj-cols $data (range (nrow $data)))))
(view (conj-cols data (range (nrow data)))) ;same as before

(def results (conj-cols data (:fitted lm)))
(col-names results)
;; => [:speed :dist 2]
(def results (col-names data [:speed :dist :predicted-dist])) ;doesn't work
(def results (-> (conj-cols data (:fitted lm) (:residuals lm))
                 (col-names [:speed :dist :predicted :residuals]))) ;nor this

($where {:speed 10.0} data)
($where {:speed {:gt 10 :lt 13}} data)

;; mongodb is probably not going to be useful as the free version is only 512M in the cloud
;; it is questionable at this stage whether it is worth installing on manjaro
;; we have postgresql or sqlite to try instead

